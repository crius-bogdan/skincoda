import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';
import qs from "qs";

const  is_dev = process.env.NODE_ENV !== 'production';
const HOST = is_dev ? '/api/' : process.env.NUXT_ENV_API_HOST;

const STATE = immutableMap({
  date: Date.now(),
  posts: [],
  categories: [],
  one_category: null,
  one_article: null
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

const structured_categories = (array) => {
  return array.map(el => {
    return {
      id: el.id,
      name: el.title
    }
  })
};
const structured_category = (el) => {
  return {
    id: el.id,
    name: el.title
  }
};
const structured_blog_posts = (array) => {
  return array.map(el => {
    return {
      id: el.id,
      slug: el.slug,
      title: el.title,
      subtitle: el.subtitle,
      cover: '/api/' + el.cover.url,
      content: el.content,
    }
  }).reverse();
};
const structured_article = (el) => {
  return {
    id: el.id,
    slug: el.slug,
    title: el.title,
    subtitle: el.subtitle,
    cover: '/api/' + el.cover.url,
    content: el.content,
    created_at: el.created_at,
    user: {
      photo: '/api/' + el.user.avatar.formats.small.url,
      name: el.user.username,
      description: el.user.description,
    },
    categories: el.tags.map(cat => {
      return {
        id: cat.id,
        name: cat.title
      }
    }),
    products: el.products.map(pr => {
      return {
        id: pr.id,
        title: pr.title,
        subtitle: pr.subtitle,
        photo: '/api/' + pr.photo.url,
        url: pr.url
      }
    })
  }
};

export const actions = {
  async load({ commit }, limit = '') {
    try {
      const limit_getter = !!limit ? '?_limit=3' : '';
      const data = await this.$axios.$get(HOST + 'blogposts' + limit_getter)

      commit('updateField', { path: 'posts', value: structured_blog_posts(data.reverse()) })
    } catch (e) {
    }
  },
  async first_load({ commit }) {
    try {
      const categories = await this.$axios.$get(HOST + 'tags/');
      const blog_posts = await this.$axios.$get(HOST + 'blogposts/');

      let structured_category = structured_categories(categories);
      let structured_blog_post = structured_blog_posts(blog_posts);

      // console.log(structured_blog_post)

      commit('updateField', { path: 'posts', value: structured_blog_post });
      commit('updateField', { path: 'categories', value: structured_category });

    } catch (e) {
      // console.log
    }
  },
  async load_by_category({ commit }, id) {
    try {
      const query = qs.stringify({ _where: { 'tags.id': id } });

      const category = await this.$axios.$get(HOST + 'tags/' + id);
      const blog_posts = await this.$axios.$get(HOST + 'blogposts?' + query);

      let structured_category = category.title;
      let structured_blog_post = structured_blog_posts(blog_posts);

      commit('updateField', { path: 'posts', value: structured_blog_post });
      commit('updateField', { path: 'one_category', value: structured_category });

    } catch (e) {
      // console.log
    }
  },
  async load_article({ commit }, slug) {
    try {
      const blog_post = await this.$axios.$get(HOST + 'blogposts?slug_eq=' + slug);
      const structured_blog_post = await structured_article(blog_post[0]);

      commit('updateField', { path: 'one_article', value: structured_blog_post });

    } catch (e) {
      // console.log
    }
  },
};
