import {Map as immutableMap} from 'immutable';
import {getField, updateField} from 'vuex-map-fields';
import qs from 'qs';

const is_dev = process.env.NODE_ENV !== 'production';
const HOST = is_dev ? '/api/' : process.env.NUXT_ENV_API_HOST;


const STATE = immutableMap({
  date: Date.now(),
  categories: [],
  ingredients: [],
  favorite_ingredients: [],
  ingredient: null,
  footer_categories: [],
  footer_list_1: [],
  footer_list_2: [],
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
  clean_ingredient(state) {
    state.ingredient = null
  },
};

const structured_categories = (array) => {
  return array.map(cat => {
    return {
      id: cat.id,
      name: cat.name
    }
  });
}
const structured_ingredients = (array) => {
  return array.map(el => {
    return {
      id: el.id,
      slug: el.slug,
      name: el.Name,
      alternative_name: el.alternative_name,
    }
  });
}
const structured_one_ingredient = (el) => {
  return {
    id: el.id,
    name: el.Name,
    alternative_name: el.alternative_name,
    categories: el.categories.map(cat => {
      return {id: cat.id, name: cat.name}
    }),
    ewg: el.ewg,
    occurence: el.occurence,
    effects_description: el.effects_description,
    science_description: el.science_description,
    comments: el.expert_comments.map(comment => {
      return {
        id: comment.id,
        name: comment.author,
        profession: comment.subtitle,
        approved: comment.approved,
        photo: HOST + comment.expert_photo.formats.small.url,
        text: comment.comment,
        rating: comment.rating,
      }
    }).filter(({approved}) => approved).reverse(),
    dry_skin: el.dry_skin,
    mature_skin: el.mature_skin,
    oily_skin: el.oily_skin,
    sensitive_skin: el.sensitive_skin,
    recommendation: el.recommendation,
    popular: el.popular,
    products: el.products.map(pr => {
      return {
        id: pr.product.id,
        title: pr.product.title,
        subtitle: pr.product.subtitle,
        price: pr.product.price,
        url: pr.product.url,
        image: HOST + pr.product.photo.url
      }
    })
  }
}

export const actions = {
  async first_load({commit}) {
    try {
      const categories = await this.$axios.$get(HOST + 'categories/');
      const ingredients = await this.$axios.$get(HOST + 'search-engine');

      commit('updateField', {path: 'categories', value: structured_categories(categories)});
      commit('updateField', {path: 'ingredients', value: structured_ingredients(ingredients)});

    } catch (e) {
    }
  },
  async load_one_ingredient({commit}, slug) {
    try {
      const ingredient = await this.$axios.$get(HOST + 'ingredients?slug_eq=' + slug);
      commit('updateField', {path: 'ingredient', value: structured_one_ingredient(ingredient[0])})
      return 200;
    } catch (e) {
      return 404
    }
  },
  async load_products_by_category({commit}, id) {
    const query = qs.stringify({_where: {'categories.id': id}});

    try {

      const data = await this.$axios.$get(HOST + 'ingredients?' + query);


      commit('updateField', {path: 'ingredients', value: structured_ingredients(data)});
    } catch (e) {

    }
  },
  async get_categories({commit}) {
    try {
      const data = await this.$axios.$get(HOST + 'categories/');

      commit('updateField', {path: 'categories', value: structured_categories(data)})
    } catch (e) {

    }
  },
  async get_ingredients({commit}) {
    try {

      const data = await this.$axios.$get(HOST + 'search-engine');

      commit('updateField', {path: 'ingredients', value: structured_ingredients(data)});
    } catch (e) {

    }
  },
  async get_popular_ingredients({commit}) {
    try {

      const data = await this.$axios.$get(HOST + 'ingredients?popular=true');

      commit('updateField', {path: 'ingredients', value: structured_ingredients(data)});
    } catch (e) {

    }
  },
  async get_ingredients_by_letter({commit}, letter) {
    const query = qs.stringify({
      _where: {
        _or: [
          [{Name_containss: letter}], // implicit AND
          [{alternative_name_containss: letter}], // implicit AND
        ],
      },
    });
    try {

      const data = await this.$axios.$get(HOST + 'ingredients' + query);

      commit('updateField', {path: 'ingredients', value: structured_ingredients(data)});
      return;
    } catch (e) {

    }
  },
  async add_comment({dispatch}, data) {
    try {
      await this.$axios.$post(HOST + 'expert-comments', data, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      dispatch('overlay/set_thanks_overlay', null, {root: true});
    } catch ({response}) {
      console.log(response);
    }
  },
  async get_favorites_ingredients({commit}) {
    try {
      const data = await this.$axios.$get(HOST + 'ingredients?favourite=true');

      commit('updateField', {path: 'favorite_ingredients', value: structured_ingredients(data)});
    } catch (e) {

    }
  },
  async get_footer_links({commit, getters}) {
    const cat = getters.getField('footer_categories');
    const list_1 = getters.getField('footer_list_1');
    const list_2 = getters.getField('footer_list_2');

    if(cat.length > 0 && list_1.length > 0 && list_2.length > 0) return

    try {

      const categories = await this.$axios.$get(HOST + 'categories/');
      const ingredients_list = await this.$axios.$get(HOST + 'ingredients?footer_list_1=true');
      const ingredients_list_2 = await this.$axios.$get(HOST + 'ingredients?footer_list_2=true');

      commit('updateField', {path: 'footer_categories', value: structured_categories(categories)})
      commit('updateField', {path: 'footer_list_1', value: structured_ingredients(ingredients_list)})
      commit('updateField', {path: 'footer_list_2', value: structured_ingredients(ingredients_list_2)})

    } catch (e) {
      console.log(e)
    }
  }
}
