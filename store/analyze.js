import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';

const  is_dev = process.env.NODE_ENV !== 'production';
const HOST = is_dev ? '/api/' : process.env.NUXT_ENV_API_HOST;

const STATE = immutableMap({
  date: Date.now(),
  analyze_name: null,
  analyze_ingredients: [],
  analyze_unknown_ingredients: [],
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

const structured_ingredients = (array) => {
  return array.map(el => {
    let desc = el.effects_description;
    if(desc.length > 105) {
      desc = el.effects_description.slice(0, 105) + '...';
    }
    return {
      id: el.id,
      name: el.Name,
      slug: el.slug,
      alternative_name: el.alternative_name,
      dry_skin: el.dry_skin,
      mature_skin: el.mature_skin,
      oily_skin: el.oily_skin,
      sensitive_skin: el.sensitive_skin,
      ewg: el.ewg,
      description: desc,
      recommendation: el.recommendation
    }
  });
};

export const actions = {
  async query_to_get_ingredients({ commit, dispatch }, { search_ingredients,  product_object }) {
    try {
      const search_engine = await this.$axios.$post(HOST + 'search-engine/search', search_ingredients);

      product_object.unknown_ingredients = JSON.stringify(search_engine.unknown);
      product_object.ingredients = search_engine.ingredients;

      const commit_data = await structured_ingredients(search_engine.ingredients);
      await dispatch('create_new_query', product_object);

      commit('updateField', { path: 'analyze_name', value: product_object.product_name});
      commit('updateField', { path: 'analyze_ingredients', value: commit_data});
      commit('updateField', { path: 'analyze_unknown_ingredients', value: search_engine.unknown});

    } catch ({ response }) {
      console.log(response)
    }
  },
  async create_new_query({ commit }, product_object) {
    try {
      await this.$axios.$post(HOST + 'queries', product_object);
    } catch (e) {
      console.log(e.response)
    }
  },
  async get_query({ commit }, slug) {
    try {
      const query_result = await this.$axios.$get(HOST + 'queries?slug_eq=' + slug);
      if(query_result.length === 0) return 404

      const data_search_engine = { ingredients: query_result[0].unformatted_ingredients };

      const search_engine = await this.$axios.$post(HOST + 'search-engine/search', data_search_engine);

      const commit_data = await structured_ingredients(search_engine.ingredients);

      commit('updateField', { path: 'analyze_name', value: query_result[0].product_name});
      commit('updateField', { path: 'analyze_ingredients', value: commit_data});
      commit('updateField', { path: 'analyze_unknown_ingredients', value: search_engine.unknown});

      return 200;

    } catch ({ response }) {
      console.log(response)
      return 404
    }
  }
};
