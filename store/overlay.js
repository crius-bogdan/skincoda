import { Map as immutableMap } from 'immutable';
import { getField, updateField } from 'vuex-map-fields';

const HOST = '/api/'

const STATE = immutableMap({
  date: Date.now(),
  overlay_field: null
});

export const state = () => STATE.toJS();

export const getters = {
  getField,
};

export const mutations = {
  updateField,
  clean(state) {
    Object.assign(state, {
      ...STATE.toJS(),
      date: Date.now(),
    });
  },
};

export const actions = {
  set_overlay_field({ commit }, val) {
    commit('updateField', { path: 'overlay_field', value: val })
  },
  set_thanks_overlay({ commit }) {
    commit('updateField', { path: 'overlay_field', value: 'OverlayThanks' })
    setTimeout(() => {
      commit('updateField', { path: 'overlay_field', value: null })
    }, 3000)
  }
};
