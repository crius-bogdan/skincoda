import {mapFields} from "vuex-map-fields";

export default {
  data() {
    return {
      search_query: '',
    }
  },
  computed: {
    ...mapFields('ingredients', [
      'ingredients',
    ]),
    search_results() {
      if(this.search_query.length > 0) return this.ingredients.filter(el => el.name.toUpperCase().includes(this.search_query.toUpperCase()))
      return []
    }
  },
}
