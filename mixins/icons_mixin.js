export default {
  props: {
    quality: {
      type: String,
      require: true
    },
    small: Boolean
  },
}
