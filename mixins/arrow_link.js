export default {
  props: {
    to: {
      type: String,
      require: true
    },
    is_orange: Boolean,
  },
}
